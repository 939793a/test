clc,close all,clear all,PS1('>> '),more off;
pkg load signal;
system('nohup sh recfifo &');
pause(1);
fi=fopen('line.raw','r');
fprintf(1,'read...\n');
fs=48000
N=2**18
win=flattopwin(N);
f=[0:N/2]*fs/N;
f(1)=NaN;
df=fs/N*2

_Ymax=[];
for m=[1:1E6]

	x=fread(fi,N,'int16');
	xmax=max(x);
	x=x/(2**15);

	rms=sqrt(sum((x-mean(x)).**2)/N);
	%rms=1;
	XMAX=20*log10(xmax);
	RMS=20*log10(rms);

	x=x.*win;

	y1=abs(fft(x))/N;
	y2=y1(1:N/2+1);
	y2(2:end-1)=2*y2(2:end-1);
	y=y2;
	
	Y=20*log10(y);
	[Ymax,i]=max(Y);
	_Ymax=[_Ymax Ymax];
	figure(2),plot(_Ymax);
	figure(1),semilogx(f,Y);
	%figure(1),plot(f,Y);

	s=sprintf('m:%4d Apeak:%5.2fdB f:%6dHz  xmax:%d rms:%4.1fdB     \r',m,Ymax,f(i),xmax,RMS );
	fprintf(1,s);
	%title(s);
	%figure(2),plot(f,Y),saveas(figure(2),sprintf('img%02d.png',n));
	axis([f(2) 24000 -120 0]);
	grid('minor','on');
	drawnow
end

fclose(fi);
close all;

