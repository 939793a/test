clc,close all,clear all,PS1('>> '),more off;
pkg load signal;
system('nohup sh recfifo &');
pause(1);
fi=fopen('line.raw','r');
fprintf(1,'read...\n');
fs=48000
N=2**16
win=flattopwin(N);
f=[0:N/2]*fs/N;
f(1)=NaN;
f(2)
df=fs/N*2

_Ymax=[];
for m=[1:1E6]
	tc=time();
	x=fread(fi,N,'int16');
	tp=time();
	xmax=max(x);
	x=x/(2**15);

	rms=sqrt(sum((x-mean(x)).**2)/N);
	%rms=1;
	XMAX=20*log10(xmax);
	RMS=20*log10(rms);

	x=x.*win;

	y1=abs(fft(x))/N;
	y2=y1(1:N/2+1);
	y2(2:end-1)=2*y2(2:end-1);
	y=y2;
	
	Y=20*log10(y);
	[Ymax,i]=max(Y);
	_Ymax=[_Ymax Ymax];
	%figure(2),plot(_Ymax);
	figure(1),semilogx(f,Y);
	%figure(1),plot(f,Y);
	%title(s);
	%figure(2),plot(f,Y),saveas(figure(2),sprintf('img%02d.png',n));
	axis([f(2) 24000 -120 0]);
	grid('minor','on');
	drawnow
	tp=time-tp;
	tc=time-tc;
	s=sprintf('%d LVL:%5.2fdB f:%2.1fHz MAX:%d RMS:%4.1fdB tc:%3.2f tp:%3.2f \r',m,Ymax,f(i),xmax,RMS,tc,tp );
	fprintf(1,s);
end

fclose(fi);
close all;

