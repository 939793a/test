
pkg load signal;
fi=fopen('line.raw','r');
fs=48000
N=2**16
win=flattopwin(N);
f=[1:N]/N/2*fs;
figure(1),grid on,plot([1 3 2]);
pause

for n=1:10

	w=fread(fi,N,'int16');
	w=w.*win;
	W=fft(w,N);
	W=W(1:N/2-1);
	W=20*log(abs(W));
	semilogx(W);
	s=sprintf('%d',n), title(s);
	drawnow
end

fclose(fi);

